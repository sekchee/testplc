# 1) Sign APK with key, if need system root authority

in AndroidManifest.xml, add
```
android:sharedUserId="android.uid.system"
```

build the unsigned APK, in terminal, use apksigner to generate the signed APK
java -jar signapk.jar is not working, will have corrupted package when install
the platform.pk8, platform.x509.pem are the key files provided by android board manufacturer
```
apksigner sign --key .\keystore\platform.pk8 --cert .\keystore\platform.x509.pem --out .\app\build\outputs\apk\release\app-release.apk .\app\build\outputs\apk\release\app-release-unsigned.apk
```

apksigner is in Android SDK build tools, need to add to environment variables so can be accessed from terminal
```
C:\Users\sekch\AppData\Local\Android\Sdk\build-tools\30.0.3
```

React Native will sign the APK with debug keystore, normally it is in folder
```
\PROJECT_FOLDER\android\app\debug.keystore
```

React Native will add the signing configs in app\build.gradle, where release will share the debug.keystore too
In android board this is not working because we need manufacturer key for system root authority
```
signingConfigs {
    debug {
        storeFile file('debug.keystore')
        storePassword 'android'
        keyAlias 'androiddebugkey'
        keyPassword 'android'
    }
}
buildTypes {
    debug {
        signingConfig signingConfigs.debug
    }
    release {
        // Caution! In production, you need to generate your own keystore file.
        // see https://facebook.github.io/react-native/docs/signed-apk-android.
        signingConfig signingConfigs.debug
        minifyEnabled enableProguardInReleaseBuilds
        proguardFiles getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro"
    }
}
```

# 2) Android read/write to UART/RS232 serial port

Android SDK does not provide any API for reading and writing to the Linux TTY serial ports.
To read/write on the TTY serial ports, we need to access linux kernel C code
using Android NDK, CMake, Java JNI. In old google code, they are using ndk-build, instead of CMake

In .\app\build.gradle file, add config to point to the cmake script file, CMakeLists.txt
```
externalNativeBuild {
    cmake {
        path file('src/main/cpp/CMakeLists.txt')
        version '3.10.2'
    }
}
```

In the .cpp file, need to follow the naming convention, so it can be mapped to Java class functions
```
extern "C" JNIEXPORT jobject JNICALL Java_com_luuwu_serialport_SerialPort_open
        (JNIEnv *env, jclass thiz, jstring path, jint baudrate, jint flags)
```
extern "C" is required, as in old google example, this is missing, and will cause function not found error
```
jclass SerialPortClass = env->GetObjectClass(thiz);
//old google code
jclass SerialPortClass = (*env)->GetObjectClass(thiz);
```
env function is accessed like shown in the example, the old google code is using pointer to access
where will have compile error

function name always start with Java, follow by com.luuwu.serialport (package name),
SerialPort (class name), and open (function name)

```
// JNI
private native static FileDescriptor open(String path, int baudrate, int flags);
public native void close();
static {
    System.loadLibrary("serial_port");
}
```
In the com.luuwu.serialport.SerialPort java class, need to add the System.loadLibrary
"serial_port" is the library name defined in CMakeLists.txt
open and close defined as native java function, which will map to functions defined in SerialPort.cpp

In this way, we can access to kernel native function if it is not supported by Android SDK.

References:
```
https://code.google.com/archive/p/android-serialport-api/
```
